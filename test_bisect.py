import re
from datetime import timedelta
from math import log2
from os import environ
from os.path import dirname, join
from pathlib import Path
from subprocess import PIPE, run

from pytest_subtests import SubTests

CHECK_LINE_REGEX = re.compile(r"^Number of entries checked: (\d+)\.$", re.MULTILINE)

ALWAYS_MISMATCHING_CODE = "false"
ALWAYS_MATCHING_CODE = "true"

SCRIPT_PATH = join(dirname(__file__), "bisect.bash")

DEBUG_ENVIRONMENT = {**environ, "DEBUG": "1"}
TRACE_ENVIRONMENT = {**environ, "DEBUG": "2"}

SCRIPT_TIMEOUT = timedelta(seconds=1).total_seconds()


def test_should_print_first_line_if_all_lines_are_insertion_points(
    subtests: SubTests, tmp_path: Path
) -> None:
    for line_count in range(1, 5):
        lines = [str(line).encode() + b"\n" for line in range(1, line_count + 1)]
        input_path = tmp_path / __name__
        input_path.write_bytes(b"".join(lines))

        result = run(
            [SCRIPT_PATH, ALWAYS_MISMATCHING_CODE, str(input_path)],
            stdout=PIPE,
            stderr=PIPE,
            check=True,
            timeout=SCRIPT_TIMEOUT,
        )

        with subtests.test(msg=f"Standard output; {line_count} lines"):
            assert result.stdout == lines[0], lines

        with subtests.test(msg=f"Standard error; {line_count} lines"):
            assert result.stderr == b""


def test_should_indicate_error_if_there_is_no_insertion_point(
    subtests: SubTests, tmp_path: Path
) -> None:
    for line_count in range(1, 5):
        lines = [str(line).encode() + b"\n" for line in range(1, line_count + 1)]
        input_path = tmp_path / __name__
        input_path.write_bytes(b"".join(lines))
        result = run(
            [SCRIPT_PATH, ALWAYS_MATCHING_CODE, str(input_path)],
            stdout=PIPE,
            stderr=PIPE,
            check=False,
            timeout=SCRIPT_TIMEOUT,
        )

        with subtests.test(msg=f"Return code; {line_count} lines"):
            assert (
                result.returncode == 3
            ), f"stdout: {result.stdout.decode()}\nstderr: {result.stderr.decode()}"

        with subtests.test(msg=f"Standard output; {line_count} lines"):
            assert result.stdout == b"", result.stderr

        with subtests.test(msg=f"Standard error; {line_count} lines"):
            assert result.stderr == b"No insertion point found.\n"


def test_should_indicate_entries_checked_in_debug_mode(tmp_path: Path) -> None:
    input_path = tmp_path / __name__
    input_path.write_bytes(b"first\nsecond\nthird\n")
    result = run(
        [SCRIPT_PATH, ALWAYS_MATCHING_CODE, str(input_path)],
        stderr=PIPE,
        env=DEBUG_ENVIRONMENT,
        check=False,
        timeout=SCRIPT_TIMEOUT,
    )

    assert result.stderr.decode().splitlines() == [
        "Remaining entry count: 3.",
        "Remaining entry count: 1.",
        "No insertion point found.",
        "Number of entries checked: 2.",
    ]


def test_should_perform_binary_search(subtests: SubTests, tmp_path: Path) -> None:
    for matching_lines in range(5, -1, -1):
        for mismatching_lines in range(1, 5):
            entries = range(-matching_lines, mismatching_lines + 1)

            with subtests.test(msg=f"Entries: {entries}"):
                lines = [str(entry).encode() + b"\n" for entry in entries]
                input_path = tmp_path / __name__
                input_path.write_bytes(b"".join(lines))
                max_checks = log2(len(lines)) + 2

                result = run(
                    [SCRIPT_PATH, '(("$ENTRY" < 0))', str(input_path)],
                    stderr=PIPE,
                    env=TRACE_ENVIRONMENT,
                    check=False,
                    timeout=SCRIPT_TIMEOUT,
                )

                stderr_string = result.stderr.decode()
                line_match = CHECK_LINE_REGEX.search(stderr_string)
                assert line_match is not None, stderr_string
                entries_checked = int(line_match.group(1))

                assert entries_checked <= max_checks, stderr_string


def test_should_print_progress_in_trace_mode(tmp_path: Path) -> None:
    input_path = tmp_path / __name__
    input_path.write_bytes(b"first\nsecond\nthird\n")
    result = run(
        [SCRIPT_PATH, ALWAYS_MATCHING_CODE, str(input_path)],
        stderr=PIPE,
        env=TRACE_ENVIRONMENT,
        check=False,
        timeout=SCRIPT_TIMEOUT,
    )

    assert result.stderr.decode().splitlines() == [
        "Remaining entry count: 3.",
        "First split file entry count: 2.",
        "Second split file entry count: 1.",
        "Checking entry: “second”.",
        "Command successful.",
        "Remaining entry count: 1.",
        "First split file entry count: 1.",
        "Checking entry: “third”.",
        "Command successful.",
        "No insertion point found.",
        "Number of entries checked: 2.",
    ]


def test_should_keep_input_file(tmp_path: Path) -> None:
    input_path = tmp_path / __name__
    input_path.write_bytes(b"first\nsecond\nthird\n")
    run(
        [SCRIPT_PATH, ALWAYS_MISMATCHING_CODE, str(input_path)],
        check=True,
        timeout=SCRIPT_TIMEOUT,
    )

    assert input_path.exists()
