let
  sources = import ./nix/sources.nix;
  pkgs = import sources.nixpkgs {};
  python = pkgs.python3.withPackages (ps: [
    ps.pytest
    ps.pytest-subtests
    ps.setuptools # Workaround for https://youtrack.jetbrains.com/issue/PY-48909/PyCharm-assumes-the-presence-of-setuptools-and-pip
  ]);
in
  pkgs.mkShell {
    packages = [
      pkgs.alejandra
      pkgs.bashInteractive
      pkgs.black
      pkgs.cacert
      pkgs.check-jsonschema
      pkgs.deadnix
      pkgs.editorconfig-checker
      pkgs.gitFull
      pkgs.gitlint
      pkgs.isort
      pkgs.mypy
      pkgs.niv
      pkgs.nodePackages.prettier
      pkgs.pre-commit
      pkgs.pylint
      pkgs.shellcheck
      pkgs.shfmt
      pkgs.statix
      pkgs.yq-go
      python
    ];
    shellHook = ''
      ln --force --no-target-directory --symbolic "${python}/bin/python" python
    '';
  }
