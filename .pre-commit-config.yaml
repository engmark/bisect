---
# Configuration file for pre-commit (https://pre-commit.com/).
# Please run `pre-commit run --all-files` when adding or changing entries.

repos:
  - repo: local
    hooks:
      - id: alejandra
        name: alejandra (Nix)
        entry: alejandra
        files: \.nix$
        language: system
        stages: [commit]

      # TODO: Remove if https://github.com/pre-commit/identify/issues/350 is fixed
      - id: bash_other
        name: bash other
        entry: bash
        args: [-o, noexec]
        files: ^\.envrc$
        language: system
        stages: [commit]

      - id: black
        name: black
        entry: black
        types: [python]
        language: system
        stages: [commit]

      - id: check-gitlab-ci
        name: check-gitlab-ci
        entry: check-jsonschema
        args: [--builtin-schema, vendor.gitlab-ci, --data-transform, gitlab-ci]
        files: ^\.gitlab-ci.yml$
        language: system
        stages: [commit]

      - id: deadnix
        name: deadnix
        entry: deadnix
        args: [--edit, --fail]
        files: \.nix$
        exclude: ^nix/sources\.nix$
        language: system
        stages: [commit]

      - id: editorconfig-checker
        name: Check .editorconfig rules
        entry: editorconfig-checker
        types: [text]
        require_serial: true
        language: system
        stages: [commit]

      - id: gitlint
        name: gitlint
        entry: gitlint
        args: [--fail-without-commits, --staged, --msg-filename]
        language: system
        stages: [commit-msg]

      - id: isort
        name: isort
        entry: isort
        types: [python]
        language: system
        stages: [commit]

      - id: mypy
        name: mypy
        entry: mypy
        types: [python]
        require_serial: true
        language: system
        stages: [commit]

      - id: pathchk
        name: pathchk
        entry: pathchk
        args: [--portability]
        exclude: ^(\.pre-commit-config\.yaml|CODE_OF_CONDUCT\.md)$
        language: system
        stages: [commit]

      - id: prettier
        name: Prettier
        entry: prettier
        args: [--ignore-unknown, --list-different, --write]
        types: [text]
        language: system
        stages: [commit]

      - id: pylint
        name: pylint
        entry: pylint
        types: [python]
        language: system
        stages: [commit]

      - id: shellcheck
        name: shellcheck
        entry: shellcheck
        types: [shell]
        language: system
        stages: [commit]

      # TODO: Remove if https://github.com/pre-commit/identify/issues/350 is fixed
      - id: shellcheck_other
        name: shellcheck other
        entry: shellcheck
        args: [--external-sources]
        files: ^\.envrc$
        language: system
        stages: [commit]

      - id: shfmt
        name: Format shell scripts
        entry: shfmt
        args: [--case-indent, --indent=4, --list, --space-redirects, --write]
        types: [shell]
        language: system
        stages: [commit]

      # TODO: Remove if https://github.com/pre-commit/identify/issues/350 is fixed
      - id: shfmt
        name: Format bash shell scripts
        entry: shfmt
        args: [--case-indent, --indent=4, --list, --space-redirects, --write]
        files: ^\.envrc$
        language: system
        stages: [commit]

      - id: statix
        name: statix
        entry: statix
        args: [check, --ignore=nix/sources.nix]
        files: \.nix$
        pass_filenames: false
        language: system
        stages: [commit]

  - repo: https://gitlab.com/engmark/shellcheck-gitlab-ci-scripts-hook
    rev: 174fda8f384db229aca5b40380e713d0c25a1cb9 # frozen: v1
    hooks:
      - id: shellcheck-gitlab-ci-scripts
        files: ^\.gitlab-ci\.yml$

  - repo: https://gitlab.com/engmark/sort-hook
    rev: 0a02a53300766e593f17f5cb4670ee8eb208bd81 # frozen: v2
    hooks:
      - id: sort
        types: [gitignore]
        stages: [commit]

  - repo: meta
    hooks:
      - id: check-hooks-apply
      - id: check-useless-excludes
